# Periphery
## Contracts

### Avalanche Testnet

```bash
# deploy router
npx hardhat --network avax_testnet deployRouter \
    --factory-address "0x62a52483EB1DcB706054Ae6164f1221abE9f5aa8" \
    --weth-address "0x1C226af1aB4b29e53BfDA6ffF34A1894BAeF6c9F"

npx hardhat --network avax_testnet verify \
    "0x92c043B5F8eD5122881073d0b5c4A05AF4839A3C" \
    "0x62a52483EB1DcB706054Ae6164f1221abE9f5aa8" \
    "0x1C226af1aB4b29e53BfDA6ffF34A1894BAeF6c9F"
```

- UniswapV2Router: `0x30e34C412E9111CCae4d9Dc2e21248Fc8C9766E6`
    - https://testnet.snowtrace.io/address/0x92c043B5F8eD5122881073d0b5c4A05AF4839A3C#code
